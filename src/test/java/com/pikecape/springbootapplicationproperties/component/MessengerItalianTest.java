package com.pikecape.springbootapplicationproperties.component;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;

/**
 * MessengerItalianTest.
 * 
 * @author Mika J. Korpela
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Messenger.class})
@ActiveProfiles(value = {"ita"})
public class MessengerItalianTest
{
    public MessengerItalianTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Autowired
    private Messenger messenger;

    /**
     * Test of getMessage method, of class Messenger.
     */
    @Test
    public void testGetMessage()
    {
        System.out.println("getMessage");
        assertEquals("Ciao Mundo expected.", "Ciao Mundo", messenger.getMessage());
    }
}
