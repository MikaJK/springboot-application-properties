package com.pikecape.springbootapplicationproperties.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * MessageRunner.
 * 
 * @author Mika J. Korpela
 */
@Component
public class MessageRunner implements CommandLineRunner
{
    @Autowired
    private Messenger messenger;
    
    /**
     * Run.
     * 
     * @param args
     * @throws Exception 
     */
    @Override
    public void run(String[] args) throws Exception
    {
        // run messenger.
        String message = this.messenger.getMessage();

        // print message.
        System.out.println(message);
    }
}
