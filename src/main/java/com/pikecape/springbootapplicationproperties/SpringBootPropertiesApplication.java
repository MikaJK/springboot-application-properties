package com.pikecape.springbootapplicationproperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBootPropertiesApplication.
 * 
 * @author Mika J. Korpela
 */
@SpringBootApplication
public class SpringBootPropertiesApplication
{

    /**
     * Main.
     * 
     * @param args 
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBootPropertiesApplication.class, args);
    }
}
