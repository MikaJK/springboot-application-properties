package com.pikecape.springbootapplicationproperties.component;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;

/**
 * MessengerFinnishTest.
 * 
 * @author Mika J. Korpela
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Messenger.class})
@ActiveProfiles(value = {"fin"})
public class MessengerFinnishTest
{
    public MessengerFinnishTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Autowired
    private Messenger messenger;

    /**
     * Test of getMessage method, of class Messenger.
     */
    @Test
    public void testGetMessage()
    {
        System.out.println("getMessage");
        assertEquals("Hei Maailma expected", "Hei Maailma", messenger.getMessage());
    }
}
