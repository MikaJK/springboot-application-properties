package com.pikecape.springbootapplicationproperties.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Messenger.
 *
 * @author Mika J. Korpela
 */
@Component
public class Messenger {
    @Value("${message.text}")
    private String messageText;

    /**
     * Get message.
     */
    public String getMessage() {
        return messageText;
    }
}
